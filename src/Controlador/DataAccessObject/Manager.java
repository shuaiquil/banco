package Controlador.DataAccessObject;

import Controlador.BusinessObject.Cliente;
import Controlador.BusinessObject.Cuenta;
import Controlador.BusinessObject.CuentaAhorro;
import Controlador.BusinessObject.CuentaCorriente;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Sol
 */
public class Manager {

    // Atributos
    private Connection cnx = null;
    private static Statement st = null; 
    private static ResultSet rs = null; 
    private static ResultSetMetaData rsMd = null; 
    private static ArrayList<Cliente> clientes = new ArrayList<>();
    private static ArrayList<CuentaAhorro> cuentaAhorro = new ArrayList<>();
    private static ArrayList<CuentaCorriente> cuentaCorriente = new ArrayList<>();

    // Constructor
    public Manager(Connection pCon) {
        cnx = pCon;
        ClientesSQL();
        CajaDeAhorroSQL();
        CuentaCorrienteSQL();
    }

    // Consultas SELECT Cliente
    private void ClientesSQL() {
        try {
            st = cnx.createStatement();
            clientes.clear();
            // Consulta SELECT 
            String CL_SELECT = "SELECT personas.pk_id_persona, personas.nombre, personas.apellido, personas.documento, personas.domicilio, clientes.pk_id_cliente, clientes.usuario, clientes.activo, pin.clave FROM personas INNER JOIN clientes ON (personas.pk_id_persona = clientes.fk_id_persona) INNER JOIN pin ON(clientes.fk_id_pin = pin.pk_id_pin)";
            //Ejecuta la consulta 
            rs = st.executeQuery(CL_SELECT);

            while (rs.next()) {
                Cliente _cli = new Cliente(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getInt(6), rs.getString(7), rs.getBoolean(8), rs.getInt(9));
                clientes.add(_cli);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    // Consultas SELECT Caja de Ahorro
    private void CajaDeAhorroSQL() {
        try {
            st = cnx.createStatement();
            cuentaAhorro.clear();

            String CA_SELECT = "SELECT cuentas.pk_id_cuenta,cuentas.fk_id_cliente, cuentas.saldo, cuentas.tipo, cuenta_ahorro.pk_id_cuenta_ahorro, cuenta_ahorro.interes FROM cuentas INNER JOIN cuenta_ahorro ON(cuentas.pk_id_cuenta = cuenta_ahorro.fk_id_cuenta)";

            rs = st.executeQuery(CA_SELECT);

            while (rs.next()) {
                CuentaAhorro _ca = new CuentaAhorro(rs.getInt(1), rs.getInt(2), rs.getDouble(3), rs.getString(4), rs.getInt(5), rs.getDouble(6));
                cuentaAhorro.add(_ca);
            }

        } catch (SQLException ex) {
            System.err.print(ex.getMessage());
        }
    }

    // Consultas SELECT Cuenta Corriente
    private void CuentaCorrienteSQL() {
        try {
            st = cnx.createStatement();
            cuentaCorriente.clear();

            String CC_SELECT = "SELECT cuentas.pk_id_cuenta,cuentas.fk_id_cliente, cuentas.saldo, cuentas.tipo, cuenta_corriente.pk_id_cuenta_corriente, cuenta_corriente.sobre_giro FROM cuentas INNER JOIN cuenta_corriente ON(cuentas.pk_id_cuenta = cuenta_corriente.fk_id_cuenta)";

            rs = st.executeQuery(CC_SELECT);

            while (rs.next()) {
                CuentaCorriente _cc = new CuentaCorriente(rs.getInt(1), rs.getInt(2), rs.getDouble(3), rs.getString(4), rs.getInt(5), rs.getDouble(6));
                cuentaCorriente.add(_cc);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    // Metodos Getters
    public ArrayList<Cliente> getClientes() {
        return clientes;
    }

    public ArrayList<CuentaAhorro> getCuentasAhorro() {
        return cuentaAhorro;
    }

    public ArrayList<CuentaCorriente> getCuentasCorriente() {
        return cuentaCorriente;
    }

    // Tabla que trae todas las cuentas del Cliente seleccionado
    public ArrayList<Object> llenarTabla(Cliente pCliente) {

        Cliente _cliente = pCliente;
        ArrayList<Object> _datos = new ArrayList<Object>();

        try {

            String sqlTM = ("SELECT  cuentas.pk_id_cuenta,cuentas.tipo, cuentas.saldo FROM clientes INNER JOIN cuentas on ( clientes.PK_ID_CLIENTE =cuentas.FK_ID_CLIENTE) " + "WHERE clientes.pk_id_cliente = " + _cliente.getIdCliente());

            rs = st.executeQuery(sqlTM);
            rsMd = rs.getMetaData(); //se asigna a su referencia el metodo que obtiene num, tipo y propiedades de columnas de un Resultset
            int cantidadColumnas = rsMd.getColumnCount(); // devuelve el numero de columnas 

            // Utilizo un bucle While para recorrer los datos de la consulta 
            while (rs.next()) {

                Object[] filas = new Object[cantidadColumnas];
                for (int i = 0; i < cantidadColumnas; i++) {
                    filas[i] = rs.getObject(i + 1);
                }
                _datos.add(filas);
            }

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return _datos;
    }

    // Tabla que trae los movimientos de la Cuenta Seleccionada
    public ArrayList<Object> llenarTablaMovimientos(Cuenta pCuenta) {
        Cuenta _cuenta = pCuenta;
        ArrayList<Object> _datos = new ArrayList<Object>();

        try {

            String sqlTM = "SELECT  movimientos.fecha_movimiento, movimientos.importe, tipo_movimiento.descripcion FROM tipo_movimiento INNER JOIN movimientos on (tipo_movimiento.pk_id_tipo_movimiento = movimientos.fk_id_tipo_movimiento)  WHERE movimientos.fk_id_cuenta = " + _cuenta.getIdCuenta();
            rs = st.executeQuery(sqlTM);
            rsMd = rs.getMetaData();//se asigna a su referencia el metodo que obtiene num, tipo y propiedades de columnas de un Resultset
            int cantidadColumnas = rsMd.getColumnCount();// devuelve el numero de columnas 
            
            // Utilizo un bucle While para recorrer los datos de la consulta 
            while (rs.next()) {
                Object[] filas = new Object[cantidadColumnas];
                for (int i = 0; i < cantidadColumnas; i++) {
                    filas[i] = rs.getObject(i + 1);
                }
                _datos.add(filas);
            }
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
        return _datos;
    }

    // Actualizacion de los montos de la Cuenta 
    public void modificarMontos(Cuenta pCuenta) {
        try {
            Cuenta cuenta;
            cuenta = pCuenta;
            st = cnx.createStatement();

            if (cuenta instanceof CuentaAhorro) {

                CuentaAhorro ca = (CuentaAhorro) cuenta;
                String SqlCA = "UPDATE cuentas INNER JOIN cuenta_ahorro  on (cuenta_ahorro.fk_id_cuenta = cuentas.pk_id_cuenta) SET saldo = " + ca.getBalance() + " WHERE cuenta_ahorro.fk_id_cuenta = " + ca.getIdCuenta();
                st.executeUpdate(SqlCA);

            } else if (cuenta instanceof CuentaCorriente) {

                CuentaCorriente cc = (CuentaCorriente) cuenta;
                String SqlCC = "UPDATE cuentas INNER JOIN cuenta_corriente on(cuenta_corriente.fk_id_cuenta = cuentas.pk_id_cuenta) SET saldo = " + cc.getBalance() + " WHERE cuenta_corriente.fk_id_cuenta = " + cc.getIdCuenta();
                st.executeUpdate(SqlCC);

            }
        } catch (SQLException ex) {
            System.err.print(ex);
        }
    }
}
