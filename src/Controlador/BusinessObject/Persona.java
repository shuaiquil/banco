package Controlador.BusinessObject;

/**
 *
 * @author Sol
 */
public class Persona {
    
    // Atributos
    private int idPersona;
    private String nombre;
    private String apellido;
    private String documento;
    private String domicilio;
    
    // Constructor
    public Persona(int pIdPersona, String pNombre, String pApellido, String pDocumento,String pDomicilio){
        idPersona = pIdPersona;
        nombre = pNombre;
        apellido = pApellido;
        documento = pDocumento;
        domicilio = pDomicilio;
    }
    
    // Metodos Getters
    public int getIdPersona(){
        return idPersona;
    }
    
    public String getNombre(){
        return nombre;
    }
    
    public String getApellido(){
        return apellido;
    }
    
    public String getDocumento(){
        return documento;
    }
    
    public String getDomicilio(){
        return domicilio;
    }
}
