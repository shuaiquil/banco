package Controlador.BusinessObject;

import java.util.ArrayList;

/**
 *
 * @author Sol
 */
public class Cliente extends Persona {

    // Atributos
    private int idCliente;
    private String usuario;
    private boolean activo;
    private int clave;
    private ArrayList<Cuenta> cuentas = new ArrayList<>();

    // Constructor
    public Cliente(int pIdPersona, String pNombre, String pApellido, String pDocumento, String pDomicilio, int pIdCliente, String pUsuario, Boolean pActivo, int pClave) {
        super(pIdPersona, pNombre, pApellido, pDocumento, pDomicilio);
        idCliente = pIdCliente;
        usuario = pUsuario;
        activo = pActivo;
        clave = pClave;
    }

    // Metodo Set
    public void agregarCuenta(Cuenta pCuenta) {
        cuentas.add(pCuenta);
    }

    // Metodos Getters
    public int getIdCliente() {
        return idCliente;
    }

    public String getUsuario() {
        return usuario;
    }

    public boolean isActivo() {
        return activo;
    }

    public int getClave() {
        return clave;
    }
    
     public ArrayList<Cuenta> getCuentas() {
        return cuentas;
    }
}
