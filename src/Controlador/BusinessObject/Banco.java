package Controlador.BusinessObject;

import Modelo.ConexionBD;
import Controlador.DataAccessObject.Manager;
import java.util.ArrayList;
import vista.IngresoClientes;

/**
 *
 * @author Sol
 */
public class Banco {

    private static ArrayList<Cliente> cliente = new ArrayList<>();
    public static ConexionBD con = new ConexionBD();
    public static Manager manager = new Manager(con.getConnection());

    private Banco() {

    }

    public static void crearBanco() {
        cliente = manager.getClientes();

        for (int i = 0; i < manager.getClientes().size(); i++) {
            for (int j = 0; j < manager.getCuentasAhorro().size(); j++) {
                if (cliente.get(i).getIdCliente() == manager.getCuentasAhorro().get(j).getIdCliente()) {
                    cliente.get(i).agregarCuenta(manager.getCuentasAhorro().get(j));
                }
            }

            for (int j = 0; j < manager.getCuentasCorriente().size(); j++) {
                if (cliente.get(i).getIdCliente() == manager.getCuentasCorriente().get(j).getIdCliente()) {
                    cliente.get(i).agregarCuenta(manager.getCuentasCorriente().get(j));
                }
            }

        }

    }

   
    public static ArrayList<Cliente> getClientes() {
        return cliente;
    }
}
