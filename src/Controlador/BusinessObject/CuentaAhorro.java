package Controlador.BusinessObject;

/**
 *
 * @author Sol
 */
public class CuentaAhorro extends Cuenta {

    // Atributos
    private int idCuentaAhorro;
    private double tazaDeInteres;

    // Constructor
    public CuentaAhorro(int pIdCuenta, int pIdCliente, double pMonto, String pTipo, int pIdCuentaAhorro, double pTazaDeInteres) {
        super(pIdCuenta, pIdCliente, pMonto, pTipo);
        idCuentaAhorro = pIdCuentaAhorro;
        tazaDeInteres = pTazaDeInteres;
    }

    // Metodos sobreescritos
    @Override
    public double getBalance() {
        return balance;
    }

    @Override
    public int getIdCuenta() {
        return idCuenta;
    }

    @Override
    public void retirarDinero(double pMonto) throws Exception {
        if (pMonto <= balance) {
            balance = balance - pMonto;
        } else {
            throw new Exception("Fondos insuficientes. \nSaldo actual: " + balance);
        }
    }

    @Override
    public void realizarDeposito(double pMonto) throws Exception {
        if (pMonto > 0) {
            balance = balance + pMonto;
        } else {
            throw new Exception("Operacion incorrecta");
        }
    }

    @Override
    public int getIdCliente() {
        return idCliente;
    }

    // Metodos Getters 
    public int getIdCuentaAhorro() {
        return idCuentaAhorro;
    }

    public double getTazaDeInteres() {
        return tazaDeInteres;
    }

    public void calcularInteres() {
        balance = balance + balance * (tazaDeInteres / 100);
    }
}
