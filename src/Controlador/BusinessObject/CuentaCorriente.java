package Controlador.BusinessObject;

/**
 *
 * @author Sol
 */
public class CuentaCorriente extends Cuenta {

    // Atributos
    private int idCuentaCorriente;
    private double sobreGiro;

    //Constructor
    public CuentaCorriente(int pIdCuenta, int pIdCliente, double pMonto, String pTipo, int pIdCuentaCorriente, double pSobreGiro) {
        super(pIdCuenta, pIdCliente, pMonto, pTipo);
        idCuentaCorriente = pIdCuentaCorriente;
        sobreGiro = pSobreGiro;
    }

    // Métodos sobreescritos
    public void retirarDinero(double pMonto) throws Exception {
        if (balance < pMonto) {
            double sobreGiroNecesario = pMonto - balance;
            if (sobreGiro >= sobreGiroNecesario) {
                balance = 0;
                sobreGiro = sobreGiro - sobreGiroNecesario;
            } else {
                throw new Exception("El monto que intenta extraer supera el limite de Sobre Giro.");
            }
        } else {
            balance = balance - pMonto;
        }
    }

    @Override
    public void realizarDeposito(double pMonto) throws Exception {
        if (pMonto > 0) {
            balance = balance + pMonto;
        } else {
            throw new Exception("Operacion incorrecta");
        }
    }

    @Override
    public int getIdCuenta() {
        return idCuenta;
    }

    @Override
    public int getIdCliente() {
        return idCliente;
    }

    @Override
    public double getBalance() {
        return balance;
    }

    // Métodos Getters 
    public int getIdCuentaCorriente() {
        return idCuentaCorriente;
    }

    public double getSobreGiro() {
        return sobreGiro;
    }
}
