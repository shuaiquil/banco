package Controlador.BusinessObject;

/**
 *
 * @author Sol
 */
public class Cuenta {

    // Atributos
    protected int idCuenta;
    protected int idCliente;
    protected double balance;
    private String tipo;

    // Constructor
    public Cuenta(int pIdCuenta, int pIdCliente, double pMonto, String pTipo) {
        idCuenta = pIdCuenta;
        idCliente = pIdCliente;
        balance = pMonto;
        tipo = pTipo;
    }

    // Metodos Setters
    public void realizarDeposito(double pMonto) throws Exception {
        if (pMonto > 0) {
            balance = balance + pMonto;
        } else {
            throw new Exception("Operacion incorrecta");
        }
    }

     public void retirarDinero(double pMonto) throws Exception {
        if (pMonto <= balance) {
            balance = balance - pMonto;
        } else {
            throw new Exception("Fondos insuficientes. \nSaldo actual: " + balance);
        }
    }

    // Metodos Getters
    public int getIdCuenta() {
        return idCuenta;
    }

    public double getBalance() {
        return balance;
    }

    public int getIdCliente() {
        return idCliente;
    }
    
}
