package Modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Sol
 */
public class ConexionBD {

    // Atributos
    private Connection conn;
    private static final String driver = "com.mysql.jdbc.Driver"; // Hace referencia al Driver que se importo para hacer la cnx
    private static final String user = "root"; // Usuario de Mysql
    private static final String password = ""; // Contraseña establecida
    private static final String url = "jdbc:mysql://localhost:3306/banco"; // Referencia donde tengo almacenada la BD

    // Constructor
    public ConexionBD() {
        conn = null;
        try {
            Class.forName(driver); // Método que intenta localizar, cargar y vincular la Clase 
            conn = (Connection) DriverManager.getConnection(url, user, password); // Se crea la cnx con la bd especificada
            System.out.println("Conexion establecida");
        } catch (SQLException ex) {
            System.err.print("No es posible conectarse " + ex);
            System.exit(0);
        } catch (Exception ex) {
            System.err.print(ex.getMessage());
        }
    }
    
    // Metodo que retorna la conexion
    public Connection getConnection(){
        return conn;
    }
    
    // Metodo que desconecta la BD
    public void desconectar() throws SQLException{
        if(conn!=null){
            if(!conn.isClosed()){
                conn.close();
            }
        }
    }
}
