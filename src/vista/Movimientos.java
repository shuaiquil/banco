package vista;

import Controlador.BusinessObject.Banco;
import Controlador.BusinessObject.Cuenta;
import java.awt.Color;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Sol
 */
public class Movimientos extends javax.swing.JFrame {

    private Cuenta cuenta;

    public Movimientos(Cuenta pCuenta) {
        cuenta = pCuenta;
        this.setTitle("Movimientos");
        this.getContentPane().setBackground(Color.WHITE);
        this.setSize(500, 500);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        initComponents();
        this.iniciarTabla();
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    }

    public void iniciarTabla() {
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.addColumn("fecha");
        modelo.addColumn("importe");
        modelo.addColumn("tipo");

        ArrayList<Object> datos = new ArrayList<Object>();
        datos = Banco.manager.llenarTablaMovimientos(cuenta);
        for (int i = 0; i < datos.size(); i++) {
            modelo.addRow((Object[]) datos.get(i));
        }
        tblMovimientos.setModel(modelo);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblMovimientos = new javax.swing.JTable();
        pnlMovimientos = new javax.swing.JPanel();
        etqMovimientos = new javax.swing.JLabel();
        etqLogoBTDF = new javax.swing.JLabel();
        panelDecorativo = new javax.swing.JPanel();
        btnSalir = new javax.swing.JButton();
        btnVolver = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        tblMovimientos = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        tblMovimientos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblMovimientos.setFocusable(false);
        tblMovimientos.getTableHeader().setResizingAllowed(false);
        tblMovimientos.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tblMovimientos);

        pnlMovimientos.setBackground(new java.awt.Color(255, 255, 255));

        etqMovimientos.setFont(new java.awt.Font("Source Sans Pro", 1, 48)); // NOI18N
        etqMovimientos.setForeground(new java.awt.Color(0, 0, 0));
        etqMovimientos.setText("Mis Movimientos");

        etqLogoBTDF.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/logobtdf.png"))); // NOI18N

        panelDecorativo.setBackground(new java.awt.Color(0, 153, 204));

        javax.swing.GroupLayout panelDecorativoLayout = new javax.swing.GroupLayout(panelDecorativo);
        panelDecorativo.setLayout(panelDecorativoLayout);
        panelDecorativoLayout.setHorizontalGroup(
            panelDecorativoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        panelDecorativoLayout.setVerticalGroup(
            panelDecorativoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout pnlMovimientosLayout = new javax.swing.GroupLayout(pnlMovimientos);
        pnlMovimientos.setLayout(pnlMovimientosLayout);
        pnlMovimientosLayout.setHorizontalGroup(
            pnlMovimientosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMovimientosLayout.createSequentialGroup()
                .addContainerGap(16, Short.MAX_VALUE)
                .addComponent(etqMovimientos)
                .addGap(18, 18, 18)
                .addComponent(etqLogoBTDF)
                .addContainerGap())
            .addComponent(panelDecorativo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlMovimientosLayout.setVerticalGroup(
            pnlMovimientosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMovimientosLayout.createSequentialGroup()
                .addContainerGap(12, Short.MAX_VALUE)
                .addGroup(pnlMovimientosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlMovimientosLayout.createSequentialGroup()
                        .addComponent(etqLogoBTDF)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlMovimientosLayout.createSequentialGroup()
                        .addComponent(etqMovimientos)
                        .addGap(20, 20, 20)))
                .addComponent(panelDecorativo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        btnSalir.setForeground(new java.awt.Color(0, 0, 0));
        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/salida 64x64.png"))); // NOI18N
        btnSalir.setText("Salir");
        btnSalir.setBorder(null);
        btnSalir.setContentAreaFilled(false);
        btnSalir.setFocusable(false);
        btnSalir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSalir.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/salida 64x64.png"))); // NOI18N
        btnSalir.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/salida 72x72.png"))); // NOI18N
        btnSalir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSalir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSalirMouseClicked(evt);
            }
        });

        btnVolver.setForeground(new java.awt.Color(0, 0, 0));
        btnVolver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/volver 64x64 (1).png"))); // NOI18N
        btnVolver.setText("   Volver");
        btnVolver.setBorder(null);
        btnVolver.setContentAreaFilled(false);
        btnVolver.setFocusable(false);
        btnVolver.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnVolver.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/volver 64x64 (1).png"))); // NOI18N
        btnVolver.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/volver 64x64 (2).png"))); // NOI18N
        btnVolver.setVerifyInputWhenFocusTarget(false);
        btnVolver.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnVolver.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnVolverMouseClicked(evt);
            }
        });
        btnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVolverActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlMovimientos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addComponent(btnVolver, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(pnlMovimientos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(173, 173, 173)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnVolver, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSalirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSalirMouseClicked
        String mensaje = "¿Esta seguro que quiere salir?";
        String botones[] = {"Salir", "Cancelar"};
        int cuadroDialogo = JOptionPane.showOptionDialog(this, mensaje, "Salir", 0, 0, null, botones, this);
        if (cuadroDialogo == JOptionPane.YES_OPTION) {
            System.exit(0);
        } else if (cuadroDialogo == JOptionPane.NO_OPTION) {

        }
    }//GEN-LAST:event_btnSalirMouseClicked

    private void btnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVolverActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnVolverActionPerformed

    private void btnVolverMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnVolverMouseClicked
      this.dispose();
      OperacionesCuenta opCta = new OperacionesCuenta(cuenta);
      opCta.setVisible(true);
    }//GEN-LAST:event_btnVolverMouseClicked

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        String mensaje = "¿Desea cerrar la aplicacion?";
        String botones[] = {"SI", "NO"};
        int cuadroDialogo = JOptionPane.showOptionDialog(this, mensaje, "Salir", 0, 0, null, botones, this);
        if (cuadroDialogo == JOptionPane.YES_OPTION) {
            System.exit(0);
        } else if (cuadroDialogo == JOptionPane.NO_OPTION) {
        
        }
    }//GEN-LAST:event_formWindowClosing


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSalir;
    private javax.swing.JButton btnVolver;
    private javax.swing.JLabel etqLogoBTDF;
    private javax.swing.JLabel etqMovimientos;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panelDecorativo;
    private javax.swing.JPanel pnlMovimientos;
    private javax.swing.JTable tblMovimientos;
    // End of variables declaration//GEN-END:variables
}
