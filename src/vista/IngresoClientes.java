package vista;

import Controlador.BusinessObject.Banco;
import Controlador.BusinessObject.Cliente;
import Atxy2k.CustomTextField.RestrictedTextField;
import java.awt.Color;
import javax.swing.JOptionPane;

/**
 *
 * @author Sol
 */
public class IngresoClientes extends javax.swing.JFrame {

    private Cliente cliente;

    public IngresoClientes() {

        initComponents();
        setTitle("Ingreso al Sistema");
        setSize(700, 1000);
        setLocationRelativeTo(null);
        this.getContentPane().setBackground(Color.white);
        setResizable(false);
        Banco.crearBanco();
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        RestrictedTextField _r = new RestrictedTextField(txtClave);
        _r.setLimit(4);
        _r.setOnlyNums(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelBienvenido = new javax.swing.JPanel();
        etqBienvenido = new javax.swing.JLabel();
        txtUsuario = new javax.swing.JTextField();
        txtClave = new javax.swing.JPasswordField();
        btnIngreso = new javax.swing.JButton();
        panelBtnsIngreso = new javax.swing.JPanel();
        btnLlamar = new javax.swing.JButton();
        btnCajeros = new javax.swing.JButton();
        btnLinkToken = new javax.swing.JButton();
        etqMsjUsuario = new javax.swing.JLabel();
        etqClave = new javax.swing.JLabel();
        etqBtdf = new javax.swing.JLabel();
        etqMsjBanco = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        panelBienvenido.setBackground(new java.awt.Color(0, 153, 204));

        etqBienvenido.setFont(new java.awt.Font("Source Sans Pro", 1, 58)); // NOI18N
        etqBienvenido.setForeground(new java.awt.Color(255, 255, 255));
        etqBienvenido.setText("Bienvenido");

        javax.swing.GroupLayout panelBienvenidoLayout = new javax.swing.GroupLayout(panelBienvenido);
        panelBienvenido.setLayout(panelBienvenidoLayout);
        panelBienvenidoLayout.setHorizontalGroup(
            panelBienvenidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBienvenidoLayout.createSequentialGroup()
                .addGap(164, 164, 164)
                .addComponent(etqBienvenido)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelBienvenidoLayout.setVerticalGroup(
            panelBienvenidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelBienvenidoLayout.createSequentialGroup()
                .addContainerGap(63, Short.MAX_VALUE)
                .addComponent(etqBienvenido)
                .addGap(61, 61, 61))
        );

        txtUsuario.setBackground(new java.awt.Color(0, 153, 204));
        txtUsuario.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        txtUsuario.setForeground(new java.awt.Color(255, 255, 255));

        txtClave.setBackground(new java.awt.Color(0, 153, 204));
        txtClave.setForeground(new java.awt.Color(255, 255, 255));

        btnIngreso.setBackground(new java.awt.Color(255, 255, 255));
        btnIngreso.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        btnIngreso.setForeground(new java.awt.Color(0, 153, 204));
        btnIngreso.setText("Entrar");
        btnIngreso.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnIngresoMouseClicked(evt);
            }
        });
        btnIngreso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIngresoActionPerformed(evt);
            }
        });

        panelBtnsIngreso.setBackground(new java.awt.Color(0, 153, 204));

        btnLlamar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/telefono70x70.png"))); // NOI18N
        btnLlamar.setBorder(null);
        btnLlamar.setContentAreaFilled(false);
        btnLlamar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnLlamar.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/telefono70x70.png"))); // NOI18N
        btnLlamar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/telefono84x84.png"))); // NOI18N
        btnLlamar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        btnCajeros.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/banco70x70.png"))); // NOI18N
        btnCajeros.setBorder(null);
        btnCajeros.setContentAreaFilled(false);
        btnCajeros.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCajeros.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/banco70x70.png"))); // NOI18N
        btnCajeros.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/banco84x84.png"))); // NOI18N
        btnCajeros.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        btnLinkToken.setBackground(new java.awt.Color(0, 153, 204));
        btnLinkToken.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnLinkToken.setForeground(new java.awt.Color(255, 255, 255));
        btnLinkToken.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/segurity70x70.png"))); // NOI18N
        btnLinkToken.setAlignmentY(1.0F);
        btnLinkToken.setBorder(null);
        btnLinkToken.setContentAreaFilled(false);
        btnLinkToken.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnLinkToken.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/segurity70x70.png"))); // NOI18N
        btnLinkToken.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/segurity84x84.png"))); // NOI18N
        btnLinkToken.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnLinkToken.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLinkTokenActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelBtnsIngresoLayout = new javax.swing.GroupLayout(panelBtnsIngreso);
        panelBtnsIngreso.setLayout(panelBtnsIngresoLayout);
        panelBtnsIngresoLayout.setHorizontalGroup(
            panelBtnsIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBtnsIngresoLayout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(btnLinkToken, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnLlamar, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addComponent(btnCajeros, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );
        panelBtnsIngresoLayout.setVerticalGroup(
            panelBtnsIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBtnsIngresoLayout.createSequentialGroup()
                .addGroup(panelBtnsIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnCajeros, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
                    .addComponent(btnLinkToken, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addComponent(btnLlamar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        etqMsjUsuario.setFont(new java.awt.Font("Dialog", 1, 16)); // NOI18N
        etqMsjUsuario.setForeground(new java.awt.Color(0, 153, 204));
        etqMsjUsuario.setText("Usuario");

        etqClave.setFont(new java.awt.Font("Dialog", 1, 16)); // NOI18N
        etqClave.setForeground(new java.awt.Color(0, 153, 204));
        etqClave.setText("Clave");

        etqBtdf.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/btf3.png"))); // NOI18N

        etqMsjBanco.setFont(new java.awt.Font("Dialog", 3, 16)); // NOI18N
        etqMsjBanco.setForeground(new java.awt.Color(0, 153, 204));
        etqMsjBanco.setText("Somos tu Banco, queremos ser tu orgullo");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelBienvenido, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(panelBtnsIngreso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(247, 247, 247)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtClave, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(etqMsjUsuario)
                            .addComponent(etqClave)))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(etqBtdf, javax.swing.GroupLayout.PREFERRED_SIZE, 506, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(119, 119, 119)
                        .addComponent(etqMsjBanco))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(257, 257, 257)
                        .addComponent(btnIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelBienvenido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41)
                .addComponent(etqBtdf, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(etqMsjBanco)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 114, Short.MAX_VALUE)
                .addComponent(etqMsjUsuario)
                .addGap(18, 18, 18)
                .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addComponent(etqClave)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtClave, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(58, 58, 58)
                .addComponent(btnIngreso)
                .addGap(66, 66, 66)
                .addComponent(panelBtnsIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnLinkTokenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLinkTokenActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnLinkTokenActionPerformed

    private void btnIngresoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIngresoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnIngresoActionPerformed

    private void btnIngresoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnIngresoMouseClicked
        for (Cliente _cli : Banco.getClientes()) {
            if (txtUsuario.getText().equals(_cli.getUsuario()) && Integer.parseInt(txtClave.getText()) == (_cli.getClave())) {
                cliente = _cli;
            }
        }
        try {
            if (cliente == null) {
                throw new Exception("ACCESO DENEGADO");
            }
            this.dispose();
            SeleccionCuentas _ctas = new SeleccionCuentas(cliente);
            _ctas.setVisible(true);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Error", JOptionPane.WARNING_MESSAGE);
            txtUsuario.setText("");
            txtClave.setText("");
        }
    }//GEN-LAST:event_btnIngresoMouseClicked

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        String mensaje = "¿Desea cerrar la aplicacion?";
        String botones[] = {"SI", "NO"};
        int cuadroDialogo = JOptionPane.showOptionDialog(this, mensaje, "Salir", 0, 0, null, botones, this);
        if (cuadroDialogo == JOptionPane.NO_OPTION) {

        } else if (cuadroDialogo == JOptionPane.YES_OPTION) {
            System.exit(0);
        }
    }//GEN-LAST:event_formWindowClosing

    public static void main(String args[]) {
        try {
            java.awt.EventQueue.invokeLater(new Runnable() {
                public void run() {
                    new IngresoClientes().setVisible(true);
                }
            });
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCajeros;
    public javax.swing.JButton btnIngreso;
    private javax.swing.JButton btnLinkToken;
    private javax.swing.JButton btnLlamar;
    private javax.swing.JLabel etqBienvenido;
    private javax.swing.JLabel etqBtdf;
    private javax.swing.JLabel etqClave;
    private javax.swing.JLabel etqMsjBanco;
    private javax.swing.JLabel etqMsjUsuario;
    private javax.swing.JPanel panelBienvenido;
    private javax.swing.JPanel panelBtnsIngreso;
    public javax.swing.JPasswordField txtClave;
    public javax.swing.JTextField txtUsuario;
    // End of variables declaration//GEN-END:variables
}
