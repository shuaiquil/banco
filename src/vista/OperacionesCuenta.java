package vista;

import Controlador.BusinessObject.Banco;
import Controlador.BusinessObject.Cliente;
import Controlador.BusinessObject.Cuenta;
import Controlador.BusinessObject.CuentaAhorro;
import Controlador.BusinessObject.CuentaCorriente;
import java.awt.Color;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import static vista.SeleccionCuentas.cliente;

/**
 *
 * @author Sol
 */
public class OperacionesCuenta extends javax.swing.JFrame {
    
    private Cuenta cuenta;
    
    public OperacionesCuenta(Cuenta pCuenta) {
        
        cuenta = pCuenta;
        
        this.setTitle("Operaciones");
        this.setSize(680, 900);
        this.setLocationRelativeTo(null);
        this.getContentPane().setBackground(Color.white);
        this.setResizable(false);
        initComponents();
        this.mostrarInformacion();
        txtEntrada.setEditable(false);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    }
    
    public void mostrarInformacion() {
        if (cuenta == null) {
            etqBienvenido.setText("fs");
        }
        if (cuenta instanceof CuentaAhorro) {
            CuentaAhorro ca = (CuentaAhorro) cuenta;
            etqBienvenido.setText("CAJA DE AHORRO N° " + ca.getIdCuentaAhorro());
        } else if (cuenta instanceof CuentaCorriente) {
            CuentaCorriente cc = (CuentaCorriente) cuenta;
            etqBienvenido.setText("CUENTA CORRIENTE N° " + cc.getIdCuentaCorriente());
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        etqBienvenido = new javax.swing.JLabel();
        panelDecorativo = new javax.swing.JPanel();
        btnDepositar = new javax.swing.JButton();
        btnSaldos = new javax.swing.JButton();
        btnRetirar = new javax.swing.JButton();
        btnMovimientos = new javax.swing.JButton();
        txtEntrada = new javax.swing.JTextField();
        btnDos = new javax.swing.JButton();
        btnTres = new javax.swing.JButton();
        btnCuatro = new javax.swing.JButton();
        btnCinco = new javax.swing.JButton();
        btnSeis = new javax.swing.JButton();
        btnSiete = new javax.swing.JButton();
        btnUno = new javax.swing.JButton();
        btnNueve = new javax.swing.JButton();
        btnCero = new javax.swing.JButton();
        btnOcho = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnAnotacion1 = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        etqBienvenido.setFont(new java.awt.Font("Source Sans Pro", 1, 36)); // NOI18N
        etqBienvenido.setForeground(new java.awt.Color(0, 153, 204));

        panelDecorativo.setBackground(new java.awt.Color(0, 153, 204));
        panelDecorativo.setForeground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout panelDecorativoLayout = new javax.swing.GroupLayout(panelDecorativo);
        panelDecorativo.setLayout(panelDecorativoLayout);
        panelDecorativoLayout.setHorizontalGroup(
            panelDecorativoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        panelDecorativoLayout.setVerticalGroup(
            panelDecorativoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 24, Short.MAX_VALUE)
        );

        btnDepositar.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        btnDepositar.setForeground(new java.awt.Color(0, 153, 204));
        btnDepositar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/Sel50x43.png"))); // NOI18N
        btnDepositar.setText("   Depositar");
        btnDepositar.setBorder(null);
        btnDepositar.setContentAreaFilled(false);
        btnDepositar.setFocusable(false);
        btnDepositar.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnDepositar.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/Sel50x43.png"))); // NOI18N
        btnDepositar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/Sel58x58.png"))); // NOI18N
        btnDepositar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnDepositarMouseClicked(evt);
            }
        });

        btnSaldos.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        btnSaldos.setForeground(new java.awt.Color(0, 153, 204));
        btnSaldos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/Sel50x43.png"))); // NOI18N
        btnSaldos.setText("Saldos   ");
        btnSaldos.setBorder(null);
        btnSaldos.setContentAreaFilled(false);
        btnSaldos.setFocusable(false);
        btnSaldos.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnSaldos.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/Sel50x43.png"))); // NOI18N
        btnSaldos.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/Sel58x58.png"))); // NOI18N
        btnSaldos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSaldosMouseClicked(evt);
            }
        });

        btnRetirar.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        btnRetirar.setForeground(new java.awt.Color(0, 153, 204));
        btnRetirar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/Sel50x43.png"))); // NOI18N
        btnRetirar.setText("   Retirar");
        btnRetirar.setBorder(null);
        btnRetirar.setContentAreaFilled(false);
        btnRetirar.setFocusable(false);
        btnRetirar.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnRetirar.setInheritsPopupMenu(true);
        btnRetirar.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/Sel50x43.png"))); // NOI18N
        btnRetirar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/Sel58x58.png"))); // NOI18N
        btnRetirar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnRetirarMouseClicked(evt);
            }
        });
        btnRetirar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRetirarActionPerformed(evt);
            }
        });

        btnMovimientos.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        btnMovimientos.setForeground(new java.awt.Color(0, 153, 204));
        btnMovimientos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/Sel50x43.png"))); // NOI18N
        btnMovimientos.setText("Movimientos    ");
        btnMovimientos.setBorder(null);
        btnMovimientos.setContentAreaFilled(false);
        btnMovimientos.setFocusable(false);
        btnMovimientos.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnMovimientos.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/Sel50x43.png"))); // NOI18N
        btnMovimientos.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/Sel58x58.png"))); // NOI18N
        btnMovimientos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnMovimientosMouseClicked(evt);
            }
        });
        btnMovimientos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMovimientosActionPerformed(evt);
            }
        });

        txtEntrada.setBackground(new java.awt.Color(255, 255, 255));
        txtEntrada.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        txtEntrada.setForeground(new java.awt.Color(0, 0, 0));
        txtEntrada.setBorder(null);
        txtEntrada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtEntradaActionPerformed(evt);
            }
        });

        btnDos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num255x55.png"))); // NOI18N
        btnDos.setBorder(null);
        btnDos.setContentAreaFilled(false);
        btnDos.setFocusable(false);
        btnDos.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num255x55.png"))); // NOI18N
        btnDos.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num260x60.png"))); // NOI18N
        btnDos.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnDos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDosActionPerformed(evt);
            }
        });

        btnTres.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num355x55.png"))); // NOI18N
        btnTres.setBorder(null);
        btnTres.setContentAreaFilled(false);
        btnTres.setFocusable(false);
        btnTres.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num355x55.png"))); // NOI18N
        btnTres.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num360x60.png"))); // NOI18N
        btnTres.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnTres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTresActionPerformed(evt);
            }
        });

        btnCuatro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num455x55.png"))); // NOI18N
        btnCuatro.setBorder(null);
        btnCuatro.setContentAreaFilled(false);
        btnCuatro.setFocusable(false);
        btnCuatro.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num455x55.png"))); // NOI18N
        btnCuatro.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num460x60.png"))); // NOI18N
        btnCuatro.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnCuatro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCuatroActionPerformed(evt);
            }
        });

        btnCinco.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num555x55.png"))); // NOI18N
        btnCinco.setBorder(null);
        btnCinco.setContentAreaFilled(false);
        btnCinco.setFocusable(false);
        btnCinco.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num555x55.png"))); // NOI18N
        btnCinco.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num560x60.png"))); // NOI18N
        btnCinco.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnCinco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCincoActionPerformed(evt);
            }
        });

        btnSeis.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num655x55.png"))); // NOI18N
        btnSeis.setBorder(null);
        btnSeis.setContentAreaFilled(false);
        btnSeis.setFocusable(false);
        btnSeis.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num655x55.png"))); // NOI18N
        btnSeis.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num660x60.png"))); // NOI18N
        btnSeis.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSeis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeisActionPerformed(evt);
            }
        });

        btnSiete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num755x55.png"))); // NOI18N
        btnSiete.setBorder(null);
        btnSiete.setContentAreaFilled(false);
        btnSiete.setFocusable(false);
        btnSiete.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num755x55.png"))); // NOI18N
        btnSiete.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num760x60.png"))); // NOI18N
        btnSiete.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSiete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSieteActionPerformed(evt);
            }
        });

        btnUno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num155x55.png"))); // NOI18N
        btnUno.setBorder(null);
        btnUno.setContentAreaFilled(false);
        btnUno.setFocusable(false);
        btnUno.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num155x55.png"))); // NOI18N
        btnUno.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num160x60.png"))); // NOI18N
        btnUno.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnUno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUnoActionPerformed(evt);
            }
        });

        btnNueve.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num955x55.png"))); // NOI18N
        btnNueve.setBorder(null);
        btnNueve.setContentAreaFilled(false);
        btnNueve.setFocusable(false);
        btnNueve.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num955x55.png"))); // NOI18N
        btnNueve.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num960x60.png"))); // NOI18N
        btnNueve.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnNueve.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNueveActionPerformed(evt);
            }
        });

        btnCero.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num055x55.png"))); // NOI18N
        btnCero.setBorder(null);
        btnCero.setContentAreaFilled(false);
        btnCero.setFocusable(false);
        btnCero.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num055x55.png"))); // NOI18N
        btnCero.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num060x60.png"))); // NOI18N
        btnCero.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnCero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCeroActionPerformed(evt);
            }
        });

        btnOcho.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num855x55.png"))); // NOI18N
        btnOcho.setBorder(null);
        btnOcho.setContentAreaFilled(false);
        btnOcho.setFocusable(false);
        btnOcho.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num855x55.png"))); // NOI18N
        btnOcho.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/num860x60.png"))); // NOI18N
        btnOcho.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnOcho.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOchoActionPerformed(evt);
            }
        });

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/cancel 48x48.png"))); // NOI18N
        btnCancelar.setText("CANCELAR    ");
        btnCancelar.setBorder(null);
        btnCancelar.setContentAreaFilled(false);
        btnCancelar.setFocusable(false);
        btnCancelar.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnCancelar.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/cancel 48x48.png"))); // NOI18N
        btnCancelar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/cancel 64x64.png"))); // NOI18N
        btnCancelar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnCancelarMouseClicked(evt);
            }
        });
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnAnotacion1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/anotacion 48x48.png"))); // NOI18N
        btnAnotacion1.setText("ANOTACION    ");
        btnAnotacion1.setBorder(null);
        btnAnotacion1.setContentAreaFilled(false);
        btnAnotacion1.setFocusable(false);
        btnAnotacion1.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnAnotacion1.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/anotacion 48x48.png"))); // NOI18N
        btnAnotacion1.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/anotacion 64x64.png"))); // NOI18N
        btnAnotacion1.setVerifyInputWhenFocusTarget(false);
        btnAnotacion1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnAnotacion1MouseClicked(evt);
            }
        });

        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/salida 64x64.png"))); // NOI18N
        btnSalir.setText("Salir");
        btnSalir.setBorder(null);
        btnSalir.setContentAreaFilled(false);
        btnSalir.setFocusable(false);
        btnSalir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSalir.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/salida 64x64.png"))); // NOI18N
        btnSalir.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/salida 72x72.png"))); // NOI18N
        btnSalir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSalir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSalirMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelDecorativo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnRetirar, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(btnUno, javax.swing.GroupLayout.DEFAULT_SIZE, 99, Short.MAX_VALUE)
                                            .addComponent(btnCuatro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(btnSiete, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(btnDos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(btnOcho, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
                                            .addComponent(btnCinco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(btnCero, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(btnTres, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(btnNueve, javax.swing.GroupLayout.DEFAULT_SIZE, 95, Short.MAX_VALUE)
                                            .addComponent(btnSeis, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                    .addComponent(txtEntrada))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(btnAnotacion1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnMovimientos, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnSalir, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(btnDepositar, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnSaldos, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(38, 38, 38))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(etqBienvenido, javax.swing.GroupLayout.PREFERRED_SIZE, 417, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(125, 125, 125))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(etqBienvenido, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelDecorativo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(47, 47, 47)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSaldos, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDepositar, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnRetirar, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(btnMovimientos, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(148, 148, 148)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txtEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(33, 33, 33)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(btnDos, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnTres, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(btnUno, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(btnAnotacion1, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(btnCuatro, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnSiete, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnCinco, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnSeis, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnNueve, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnOcho, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCero, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(57, 57, 57))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void activarBotones() {
        btnDepositar.setEnabled(true);
        btnSaldos.setEnabled(true);
        btnRetirar.setEnabled(true);
        btnMovimientos.setEnabled(true);
    }
    
    public void retirarDinero() {
        try {
            if (cuenta instanceof CuentaAhorro) {
                CuentaAhorro ca = (CuentaAhorro) cuenta;
                ca.retirarDinero(Double.parseDouble(txtEntrada.getText()));
                ca.getBalance();
                Banco.manager.modificarMontos(ca);
            } else if (cuenta instanceof CuentaCorriente) {
                CuentaCorriente cc = (CuentaCorriente) cuenta;
                cc.retirarDinero(Double.parseDouble(txtEntrada.getText()));
                cc.getBalance();
                Banco.manager.modificarMontos(cc);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), " Error", JOptionPane.WARNING_MESSAGE);
            txtEntrada.setText("");
        }
    }
    
    public void depositarDinero() {
        try {
            if (cuenta instanceof CuentaAhorro) {
                CuentaAhorro ca = (CuentaAhorro) cuenta;
                ca.realizarDeposito(Double.parseDouble(txtEntrada.getText()));
                ca.getBalance();
                Banco.manager.modificarMontos(ca);
            } else if (cuenta instanceof CuentaCorriente) {
                CuentaCorriente cc = (CuentaCorriente) cuenta;
                cc.realizarDeposito(Double.parseDouble(txtEntrada.getText()));
                cc.getBalance();
                Banco.manager.modificarMontos(cc);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), " Error", JOptionPane.WARNING_MESSAGE);
            txtEntrada.setText("");
        }
    }
    private void btnRetirarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRetirarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnRetirarActionPerformed

    private void btnUnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUnoActionPerformed
        txtEntrada.setText(txtEntrada.getText() + "1");
    }//GEN-LAST:event_btnUnoActionPerformed

    private void btnNueveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNueveActionPerformed
        txtEntrada.setText(txtEntrada.getText() + "9");
    }//GEN-LAST:event_btnNueveActionPerformed

    private void btnSieteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSieteActionPerformed
        txtEntrada.setText(txtEntrada.getText() + "7");
    }//GEN-LAST:event_btnSieteActionPerformed

    private void btnCeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCeroActionPerformed
        txtEntrada.setText(txtEntrada.getText() + "0");
    }//GEN-LAST:event_btnCeroActionPerformed

    private void btnOchoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOchoActionPerformed
        txtEntrada.setText(txtEntrada.getText() + "8");
    }//GEN-LAST:event_btnOchoActionPerformed

    private void btnSeisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeisActionPerformed
        txtEntrada.setText(txtEntrada.getText() + "6");
    }//GEN-LAST:event_btnSeisActionPerformed

    private void txtEntradaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtEntradaActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_txtEntradaActionPerformed

    private void btnDepositarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDepositarMouseClicked
        btnDepositar.setEnabled(true);
        btnSaldos.setEnabled(false);
        btnRetirar.setEnabled(false);
        btnMovimientos.setEnabled(false);
    }//GEN-LAST:event_btnDepositarMouseClicked

    private void btnRetirarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnRetirarMouseClicked
        btnDepositar.setEnabled(false);
        btnSaldos.setEnabled(false);
        btnRetirar.setEnabled(true);
        btnMovimientos.setEnabled(false);
    }//GEN-LAST:event_btnRetirarMouseClicked

    private void btnSaldosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaldosMouseClicked
        this.dispose();
        SeleccionCuentas selCta = new SeleccionCuentas(cliente);
        selCta.setVisible(true);

    }//GEN-LAST:event_btnSaldosMouseClicked

    private void btnMovimientosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMovimientosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnMovimientosActionPerformed

    private void btnMovimientosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMovimientosMouseClicked
        this.dispose();
        Movimientos movimientosCta = new Movimientos(cuenta);
        movimientosCta.setVisible(true);
    }//GEN-LAST:event_btnMovimientosMouseClicked

    private void btnDosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDosActionPerformed
        txtEntrada.setText(txtEntrada.getText() + "2");
    }//GEN-LAST:event_btnDosActionPerformed

    private void btnTresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTresActionPerformed
        txtEntrada.setText(txtEntrada.getText() + "3");
    }//GEN-LAST:event_btnTresActionPerformed

    private void btnCuatroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCuatroActionPerformed
        txtEntrada.setText(txtEntrada.getText() + "4");
    }//GEN-LAST:event_btnCuatroActionPerformed

    private void btnCincoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCincoActionPerformed
        txtEntrada.setText(txtEntrada.getText() + "5");
    }//GEN-LAST:event_btnCincoActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnCancelarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCancelarMouseClicked
        txtEntrada.setText("");
        this.activarBotones();
    }//GEN-LAST:event_btnCancelarMouseClicked

    private void btnAnotacion1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAnotacion1MouseClicked
        try {
            if (txtEntrada.getText().equals("")) {
                throw new Exception("Por favor ingrese un monto para poder operar");
            }
            if (btnRetirar.isEnabled() && btnDepositar.isEnabled() && btnMovimientos.isEnabled() && btnSaldos.isEnabled()) {
                throw new Exception("Por favor seleccione una opcion para poder operar");
            }
            if (btnRetirar.isEnabled()) {
                this.retirarDinero();
            } else if (btnDepositar.isEnabled()) {
                this.depositarDinero();
            }
            JOptionPane.showMessageDialog(null, "Su transaccion ha sido finalizada\n" + "Saldo actual: " + cuenta.getBalance());
            this.txtEntrada.setText("");
            String mensaje = "¿Desea realizar otra operacion?";
            String botones[] = {"SI", "NO"};
            int cuadroDialogo = JOptionPane.showOptionDialog(this, mensaje, "Salir", 0, 0, null, botones, this);
            if (cuadroDialogo == JOptionPane.YES_OPTION) {
                this.activarBotones();
            } else if (cuadroDialogo == JOptionPane.NO_OPTION) {
                Banco.con.desconectar();
                System.exit(0);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), " Error", JOptionPane.WARNING_MESSAGE);
            txtEntrada.setText("");
        }
    }//GEN-LAST:event_btnAnotacion1MouseClicked

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        String mensaje = "¿Desea cerrar la aplicacion?";
        String botones[] = {"SI", "NO"};
        int cuadroDialogo = JOptionPane.showOptionDialog(this, mensaje, "Salir", 0, 0, null, botones, this);
        if (cuadroDialogo == JOptionPane.YES_OPTION) {
            System.exit(0);
        } else if (cuadroDialogo == JOptionPane.NO_OPTION) {
            
        }
    }//GEN-LAST:event_formWindowClosing

    private void btnSalirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSalirMouseClicked
        String mensaje = "¿Desea cerrar la aplicacion?";
        String botones[] = {"SI", "NO"};
        int cuadroDialogo = JOptionPane.showOptionDialog(this, mensaje, "Salir", 0, 0, null, botones, this);
        if (cuadroDialogo == JOptionPane.YES_OPTION) {
            System.exit(0);
        } else if (cuadroDialogo == JOptionPane.NO_OPTION) {
            
        }
    }//GEN-LAST:event_btnSalirMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAnotacion1;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCero;
    private javax.swing.JButton btnCinco;
    private javax.swing.JButton btnCuatro;
    private javax.swing.JButton btnDepositar;
    private javax.swing.JButton btnDos;
    private javax.swing.JButton btnMovimientos;
    private javax.swing.JButton btnNueve;
    private javax.swing.JButton btnOcho;
    private javax.swing.JButton btnRetirar;
    private javax.swing.JButton btnSaldos;
    private javax.swing.JButton btnSalir;
    private javax.swing.JButton btnSeis;
    private javax.swing.JButton btnSiete;
    private javax.swing.JButton btnTres;
    private javax.swing.JButton btnUno;
    private javax.swing.JLabel etqBienvenido;
    private javax.swing.JPanel panelDecorativo;
    private javax.swing.JTextField txtEntrada;
    // End of variables declaration//GEN-END:variables
}
