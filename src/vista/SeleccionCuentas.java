package vista;

import Controlador.BusinessObject.Banco;
import Controlador.BusinessObject.Cliente;
import Controlador.BusinessObject.Cuenta;
import java.awt.Color;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Sol
 */
public class SeleccionCuentas extends javax.swing.JFrame {

    static Cliente cliente;
    private Cuenta cuenta;

    public SeleccionCuentas(Cliente pCliente) {
        cliente = pCliente;
        this.setTitle("Seleccion de cuentas");
        this.getContentPane().setBackground(Color.WHITE);
        this.setSize(500, 500);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        initComponents();
        iniciarTabla();
        txtId.setVisible(false);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelMisCuentas = new javax.swing.JPanel();
        etqLogoBtdf = new javax.swing.JLabel();
        EtqMisCuentas = new javax.swing.JLabel();
        panelDecorativo = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblCuentas = new javax.swing.JTable();
        etqMensaje = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        btnSalir = new javax.swing.JButton();
        etqSalir = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        panelMisCuentas.setBackground(new java.awt.Color(255, 255, 255));
        panelMisCuentas.setForeground(new java.awt.Color(0, 153, 204));

        etqLogoBtdf.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/logobtdf.png"))); // NOI18N

        EtqMisCuentas.setFont(new java.awt.Font("Source Sans Pro", 1, 48)); // NOI18N
        EtqMisCuentas.setForeground(new java.awt.Color(0, 0, 0));
        EtqMisCuentas.setText("Mis Cuentas");

        panelDecorativo.setBackground(new java.awt.Color(0, 153, 204));
        panelDecorativo.setForeground(new java.awt.Color(0, 153, 204));

        javax.swing.GroupLayout panelDecorativoLayout = new javax.swing.GroupLayout(panelDecorativo);
        panelDecorativo.setLayout(panelDecorativoLayout);
        panelDecorativoLayout.setHorizontalGroup(
            panelDecorativoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        panelDecorativoLayout.setVerticalGroup(
            panelDecorativoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 23, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout panelMisCuentasLayout = new javax.swing.GroupLayout(panelMisCuentas);
        panelMisCuentas.setLayout(panelMisCuentasLayout);
        panelMisCuentasLayout.setHorizontalGroup(
            panelMisCuentasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMisCuentasLayout.createSequentialGroup()
                .addGap(92, 92, 92)
                .addComponent(EtqMisCuentas)
                .addGap(54, 54, 54)
                .addComponent(etqLogoBtdf)
                .addContainerGap(15, Short.MAX_VALUE))
            .addComponent(panelDecorativo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        panelMisCuentasLayout.setVerticalGroup(
            panelMisCuentasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMisCuentasLayout.createSequentialGroup()
                .addGroup(panelMisCuentasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelMisCuentasLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(etqLogoBtdf))
                    .addGroup(panelMisCuentasLayout.createSequentialGroup()
                        .addContainerGap(31, Short.MAX_VALUE)
                        .addComponent(EtqMisCuentas)
                        .addGap(29, 29, 29)))
                .addComponent(panelDecorativo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jScrollPane1.setBackground(new java.awt.Color(0, 153, 204));
        jScrollPane1.setForeground(new java.awt.Color(0, 153, 204));

        tblCuentas = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        tblCuentas.setBackground(new java.awt.Color(255, 255, 255));
        tblCuentas.setForeground(new java.awt.Color(0, 0, 0));
        tblCuentas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblCuentas.setFocusable(false);
        tblCuentas.setSelectionForeground(new java.awt.Color(0, 153, 204));
        tblCuentas.getTableHeader().setResizingAllowed(false);
        tblCuentas.getTableHeader().setReorderingAllowed(false);
        tblCuentas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblCuentasMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblCuentas);

        etqMensaje.setFont(new java.awt.Font("Source Sans Pro", 2, 16)); // NOI18N
        etqMensaje.setForeground(new java.awt.Color(0, 153, 204));
        etqMensaje.setText("Por favor seleccione una cuenta para poder operar.");

        txtId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIdActionPerformed(evt);
            }
        });

        btnSalir.setBackground(new java.awt.Color(255, 255, 255));
        btnSalir.setForeground(new java.awt.Color(0, 153, 204));
        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/salida 64x64.png"))); // NOI18N
        btnSalir.setBorder(null);
        btnSalir.setBorderPainted(false);
        btnSalir.setContentAreaFilled(false);
        btnSalir.setFocusable(false);
        btnSalir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSalir.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/salida 64x64.png"))); // NOI18N
        btnSalir.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/salida 72x72.png"))); // NOI18N
        btnSalir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSalir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSalirMouseClicked(evt);
            }
        });
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        etqSalir.setText("Salir");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelMisCuentas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(86, 86, 86)
                            .addComponent(etqMensaje))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(29, 29, 29)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(59, 59, 59))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(etqSalir)
                        .addGap(113, 113, 113))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelMisCuentas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(49, 49, 49)
                .addComponent(etqMensaje)
                .addGap(5, 5, 5)
                .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 111, Short.MAX_VALUE)
                .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(etqSalir)
                .addGap(21, 21, 21))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIdActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIdActionPerformed

    private void tblCuentasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblCuentasMouseClicked
        // Me va a tomar la fila de la seleccion que se ha presionado mediante un clickeo
        int seleccion = tblCuentas.rowAtPoint(evt.getPoint());

        txtId.setText((String) tblCuentas.getValueAt(seleccion, 0).toString());
        for (Cuenta _cu : cliente.getCuentas()) {
            if (Integer.parseInt(txtId.getText()) == _cu.getIdCuenta()) {
                cuenta = _cu;
            }
        }
        this.dispose();
        OperacionesCuenta ventanaOp = new OperacionesCuenta(cuenta);
        ventanaOp.setVisible(true);

    }//GEN-LAST:event_tblCuentasMouseClicked

    private void btnSalirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSalirMouseClicked
        String mensaje = "¿Esta seguro que quiere salir?";
        String botones[] = {"Salir", "Cancelar"};
        int cuadroDialogo = JOptionPane.showOptionDialog(this, mensaje, "Salir", 0, 0, null, botones, this);
        if (cuadroDialogo == JOptionPane.YES_OPTION) {
            System.exit(0);
        } else if (cuadroDialogo == JOptionPane.NO_OPTION) {

        }
    }//GEN-LAST:event_btnSalirMouseClicked

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSalirActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        String mensaje = "¿Desea cerrar la aplicacion?";
        String botones[] = {"SI", "NO"};
        int cuadroDialogo = JOptionPane.showOptionDialog(this, mensaje, "Salir", 0, 0, null, botones, this);
        if (cuadroDialogo == JOptionPane.YES_OPTION) {
            System.exit(0);
        } else if (cuadroDialogo == JOptionPane.NO_OPTION) {

        }
    }//GEN-LAST:event_formWindowClosing

    public void iniciarTabla() {
        // Crea un nuevo modelo de tabla
        DefaultTableModel modelo = new DefaultTableModel();

        // Se le agregan al modelo las columnas con sus respectivos nombres
        modelo.addColumn("N° DE CUENTA");
        modelo.addColumn("SALDO");
        modelo.addColumn("TIPO");

        //Se crea un ArrayList
        ArrayList<Object> datos = new ArrayList<Object>();
        // A nuestra referencia a objeto 
        datos = Banco.manager.llenarTabla(cliente);

        for (int i = 0; i < datos.size(); i++) {
            modelo.addRow((Object[]) datos.get(i));
        }

        tblCuentas.setModel(modelo);
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel EtqMisCuentas;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel etqLogoBtdf;
    private javax.swing.JLabel etqMensaje;
    private javax.swing.JLabel etqSalir;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panelDecorativo;
    private javax.swing.JPanel panelMisCuentas;
    private javax.swing.JTable tblCuentas;
    private javax.swing.JTextField txtId;
    // End of variables declaration//GEN-END:variables
}
